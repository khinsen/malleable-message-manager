;;;; Transform toot to HTML mail

(in-package #:malleable-message-manager)

;; The mail subject line contains the first 70 characters of the
;; plain-text representation of the toot. It must be encoded because
;; it may contain non-ASCII characters.

(defun subject-line (toot)
  (-<> toot
       (toot-content-as-plain-text)
       (str:substring 0 70 <>)
       (str:trim)
       (str:collapse-whitespaces)
       (q-encode-header-line)))

;; A Message-id must be unique. Combining account name and the origin id
;; should ensure that.
 
(defun message-id (toot)
  (format nil "<~a-~a@~a>"
          (id-on-origin-instance toot)
          (-> toot (tooter:account) (tooter:username))
          (->> toot
               (tooter:url)
               (str:split "/")
               (third))))

;; The From and To header lines contain the display name followed by
;; the account name, which has the same syntax as a mail
;; address. Display names frequently contain colons (usually due to
;; emoji), which have to be removed because they have a special
;; meaning in mail addresses (-> groups). Display names may also
;; require encoding if they contain non-ASCII characters.

(defun full-mail-address (account)
  (format nil "~a <~a>"
          (-<> (tooter:display-name account)
               (remove #\: <>)
               (remove #\? <>)
               (remove #\! <>)
               (remove #\, <>)
               (remove #\@ <>)
               (remove #\< <>)
               (remove #\> <>)
               (str:collapse-whitespaces)
               (q-encode-header-line))
          (full-account-name account)))

(defun mail-address (account)
  (let ((full (full-mail-address account)))
    (handler-case (progn (mel:address full) full)
      (error (c)
        (declare (ignore c))
        (full-account-name account)))))

;; If the toot is a reply, an in-reply-to header line will help mail readers
;; display the reply chain.

(defun toot-replied-to (toot)
  (when-let (id (tooter:in-reply-to-id toot))
    (handler-case (tooter:find-status (client-for-instance (instance-url toot)) id)
      (error (c) (declare (ignore c))
        nil))))

(defun in-reply-to (toot)
  (some->> toot
           (toot-replied-to)
           (message-id)))

;; Converting the HTML to a mail body is an exercice in MIME encoding.

(defun mime-attachment (url)
  (destructuring-bind (type subtype)
      (->> url
           (mime-type)
           (str:split "/"))
    (handler-case
        (make-instance
         'cl-mime:mime
         :type type
         :subtype subtype
         :encoding :base64
         :content (flexi-streams:with-output-to-sequence (s)
                    (download-url-to-stream url s)))
      (error (c) (declare (ignore c))
        nil))))

(defun mime-message (toot)
  (let* ((text
          (-<> toot
               (toot-content-as-html)
               (babel:string-to-octets <> :encoding :utf-8)
               (make-instance 'cl-mime:mime
                              :type "text" :subtype "html"
                              :content-encoding :8bit
                              :content <>
                              :encoding :quoted-printable)))
         (attachment-urls
          (->> toot
               (tooter:media-attachments)
               (mapcar #'tooter:url)))
         (card-url (some-> toot (tooter:preview-card) (tooter:image)))
         (urls (if card-url (cons card-url attachment-urls) attachment-urls)))
    
    (if urls
        (make-instance
         'cl-mime:multipart-mime
         :subtype "mixed"
         :content (cons text (remove nil (mapcar #'mime-attachment urls))))
      text)))

(defun mime-body (mime-message)
  (cl-mime:print-mime nil
                      mime-message
                      nil nil))

;; Finally, all elements can be assembled into a message object. The From header
;; contains the author of the toot. For a boost, the To header contains the
;; boosting account, otherwise it contains the user's own account.

(defun toot-as-message (status)
  (let* ((toot (toot status))
         (mime-message (mime-message toot))
         (headers (cl-mime:get-mime-headers mime-message))
         (body (mime-body mime-message))
         (message (make-instance 'mel:mime-message :folder nil)))
    (setf (mel:header-fields message)
          (if-let (reply-id (in-reply-to toot))
            (cons (cons :in-reply-to reply-id) headers)
            headers))
    (setf (mel:date message) (tooter:created-at toot))
    (setf (mel:message-id message) (message-id toot))
    (setf (mel:subject message) (subject-line toot))
    (setf (mel:from message) (mel:address (mail-address (tooter:account toot))))
    (setf (mel:to message) (if (boost? status)
                               (mail-address (tooter:account status))
                               (mel:address *mastodon-account-name*)))
    (mel:finalize-message message :body body)
    message))

;; Store a message in a mail folder, unless a message with the same id
;; already exists.

(defun message-id-in-folder? (message-id mail-folder)
  (some (lambda (m) (equal message-id (mel:message-id m)))
        (mel:messages mail-folder)))

(defun store-message-in-folder (message folder)
  (unless (message-id-in-folder? (mel:message-id message) folder)
      (mel:copy-message message folder)))
