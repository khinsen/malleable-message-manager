;; System definition

(asdf:defsystem #:malleable-message-manager
  :description "Transfers Mastodon messages to Mail or RSS"
  :author "Konrad Hinsen <konrad.hinsen@fastmail.net>"
  :license  "GPL"
  :version "0.0.1"
  :serial t
  :depends-on (:alexandria
               :arrow-macros
               :babel
               :cl-base64
               :cl-mime
               :cl-qprint
               :drakma
               :flexi-streams
               :html-inspector-views
               :local-time
               :mel-base
               :plump
               :plump-sexp
               :serapeum
               :str
               :tooter
               :vom)
  :components ((:file "optimize-for-debugging")
               (:file "package")
               (:file "mastodon-instance")
               (:file "maildir")
               (:file "mime-types")
               (:file "toots")
               (:file "foreign-toots")
               (:file "toot-formatting")
               (:file "toot-properties")
               (:file "q-encode")
               (:file "toot-to-mail")
               (:file "toot-to-rss-item")
               (:file "channels")
               (:file "dispatchers")
               (:file "rss-feeds")
               (:file "toot-registries")
               (:file "counters")
               (:file "timelines")
               (:file "bookmarks")
               (:file "inspector-views")))
