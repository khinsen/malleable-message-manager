;;;; Toots obtained from other instances

(in-package #:malleable-message-manager)

;; Accessing toots via their origin instance is the best way to
;; retrieve the full reply tree containing them. Care must be taken
;; with toot IDs, which are specific to each instance. A foreign toot
;; must therefore be stored with a reference to the instance it was
;; obtained from.

(defclass foreign-status (tooter-objects:status)
  ((instance-url :reader get-instance-url :initarg :instance-url)))

(defgeneric instance-url (status)
  (:documentation "Return the URL of the instance from which the status was obtained"))

(defmethod instance-url ((status foreign-status))
  (get-instance-url status))

(defmethod instance-url ((status tooter-objects:status))
  *mastodon-instance-url*)

;; Retrieve a status from the instance in which it was created.

(defun client-for-instance (instance-url)
  (if (equal instance-url *mastodon-instance-url*)
      *mastodon-client*
      (make-instance 'tooter:client
                     :base instance-url
                     :name "mmm")))

(defun origin-instance-url (status)
  (-<>> status
        toot
        (tooter:url)
        (str:split "/")
        (subseq <> 0 3)
        (str:join "/")))

(defun client-for-origin (status)
  (-> status
      (origin-instance-url)
      (client-for-instance)))

(defun add-instance-url (url)
  (lambda (status)
    (change-class status 'foreign-status
                  :instance-url url)
    status))

(defun find-status-from-id-and-instance (id instance-url)
  (if (equal instance-url (tooter:base *mastodon-client*))
      (tooter:find-status *mastodon-client* id)
      (let* ((client (client-for-instance instance-url))
             (status (tooter:find-status client id)))
        (funcall (add-instance-url instance-url) status))))

(defun retrieve-from-origin-instance (status)
  (let* ((instance-url (origin-instance-url status))
         (origin-id (id-on-origin-instance status)))
    (find-status-from-id-and-instance origin-id instance-url)))

(defun find-status-from-url (url)
  (let* ((url-parts (str:split "/" url))
         (id (alexandria:lastcar url-parts))
         (instance-url (str:concat (first url-parts) "//" (third url-parts))))
    (find-status-from-id-and-instance id instance-url)))

;; Retrieve a reply tree from the origin instances of each status.
;; Proceed in two phases:
;;  - first, follow the in-reply-to-id pointers to the root of the tree
;;  - second, use the context of each status to find its direct children,
;;    recursively

(defun reply-chain-from-origin-instances (toots)
  (let ((head (car toots)))
    (if-let (parent-id (tooter:in-reply-to-id head))
      (-<> parent-id
           (find-status-from-id-and-instance <> (instance-url head))
           (retrieve-from-origin-instance)
           (cons <> toots)
           (reply-chain-from-origin-instances))
      toots)))

(defun root-of-reply-tree-from-origin-instances (toot)
  (->> toot
       (retrieve-from-origin-instance)
       (list)
       (reply-chain-from-origin-instances)
       (first)))

(defun direct-descendants (toot)
  (let ((id (tooter:id toot)))
    (->> (tooter:context (client-for-origin toot) id)
         (tooter:descendants)
         (remove-if-not #'(lambda (s) (equal id (tooter:in-reply-to-id s))))
         (mapcar (add-instance-url (origin-instance-url toot)))
         (mapcar #'retrieve-from-origin-instance))))

(defun add-with-descendants (ht status)
  (let ((uri (tooter:uri status)))
    (setf (gethash uri ht) status)
    (dolist (d (direct-descendants status))
      (add-with-descendants ht d))))

(defun reply-tree-from-origin-instances (toot)
  (let* ((toots (dict)))
    (->> toot
         (root-of-reply-tree-from-origin-instances)
         (add-with-descendants toots))
    (alexandria:hash-table-values toots)))
