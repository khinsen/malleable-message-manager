;;;; RSS feeds

(in-package #:malleable-message-manager)

;; Read an RSS feed from a file and return the parsed tree, consisting
;; of node objects as defined in plump.

(defun read-rss-feed (filename)
  (with-open-file (input filename
                         :direction :input
                         :if-does-not-exist nil)
    (and input
         (let ((plump:*tag-dispatchers* plump:*xml-tags*))
           (plump:parse input)))))

;; Make a tree corresponding to an empty feed. Non-literal strings are
;; coerced to values of type simple-string. Plump assumes
;; simple-string in serialization for performance reasons.

(defun make-empty-rss-feed (title)
  (let* ((root (plump:make-root))
         (header (plump:make-xml-header root
                                        :attributes (dict "version" "1.0"
                                                          "encoding" "utf-8")))
         (rss (plump:make-element root "rss"
                                  :attributes (dict "version" "2.0")))
         (channel (plump:make-element rss "channel")))
    (-<> (plump:make-element channel "title")
         (plump:make-text-node <> title))
    (-<> (plump:make-element channel "link")
         (plump:make-text-node <> (coerce (my-account-url) 'simple-string)))
    (-<> (plump:make-element channel "description")
         (plump:make-text-node <> "Toots"))
    (-<> (plump:make-element channel "language")
         (plump:make-text-node <> "en-us"))
    root))

;; Read a feed from a file or make an empty one if the file does not exist.

(defun make-rss-feed (filename)
  (or (read-rss-feed filename)
      (make-empty-rss-feed (pathname-name filename))))

