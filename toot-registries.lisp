;;;; Toot registries

(in-package #:malleable-message-manager)

;; A toot registry stores toot ids and the date/time when they were
;; added.  A typical use case is recording which toots were already
;; presented by a client, but other use cases are easy to imagine
;; (bookmarks etc.)

(defclass toot-registry ()
  ((id-date-hashmap :reader id-date-hashmap :initform (dict))))

(defun make-toot-registry ()
  (make-instance 'toot-registry))

;; When a toot is registered, its parent (if any) is registered as well,
;; because I consider the two nearly equivalent.

(defun register (status registry)
  (let ((now (local-time:now)))
    (setf (gethash (tooter:id status) (id-date-hashmap registry)) now)
    (when-let (parent (tooter:parent status))
      (setf (gethash (tooter:id parent) (id-date-hashmap registry)) now))))

;; Retrieving the registration timestamp doubles as a test for presence,
;; as the return value is nil for an unregistered toot.

(defun registration-timestamp (status registry)
  (-<> status
       (toot)
       (tooter:id)
       (gethash <> (id-date-hashmap registry))))

;; Persistent registries would grow indefinitely if not cleaned up from
;; time to time. This function removes all toots older than a given timestamp.

(defun remove-before (timestamp registry)
  (let ((h (id-date-hashmap registry)))
    (maphash #'(lambda (k v)
                 (when (local-time:timestamp< v timestamp)
                   (remhash k h)))
             h)))

;; Toots in a Mastodon timelines have increasing ids. The highest id
;; in a registry therefore labels the toot most recent in the timeline
;; (which is not necessarily the most recently registered toot). In my
;; Mastodon client, I use a registry to remember already-seen toots,
;; allowing me to filter out multiple boosts. The max-id of that
;; registry then serves as a marker for the current position in the
;; timeline.

(defun max-id (registry)
  (-> (hash-fold #'(lambda (id date acc)
                     (declare (ignore date))
                     (let ((numerical-id (parse-integer id)))
                       (max numerical-id acc)))
                 1
                 (id-date-hashmap registry))
      (write-to-string)))
