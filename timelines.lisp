;;;; Timelines

(in-package #:malleable-message-manager)

;; Retrieve the home timeline of a client starting from from-id.

(defun timeline-from-id (client from-id)
  (apply #'append
         (reverse
          (loop for min-id = from-id then (tooter:id (car section))
                for section = (tooter:timeline client :home
                                               :min-id min-id
                                               :limit 40)
                while section
                collect section))))
