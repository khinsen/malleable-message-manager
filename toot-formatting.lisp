;;;; HTML and text representation of toots for use in RSS feeds and e-mail

(in-package #:malleable-message-manager)

;; The HTML representation starts with the toot contents, already
;; in HTML format. If the toot has a preview card, it is shown right
;; below. Next come image attachments, each followed by its textual
;; description if provided. Videos are not handled yet. At the
;; bottom, a link for opening the toot in the Mastodon Web app
;; permits interacting with the toot (replying, boosting, etc.)

(defun toot-content-as-html (status)
  (let ((toot (toot status)))
    (with-output-to-string (s)
      (write-string (tooter:content toot) s)
      (write-string "<br> " s)
      (when-let (card (tooter:preview-card toot))
        (write-string (tooter:html card) s)
        (when-let (image (tooter:image card))
          (format s "<img src=\"~a\">" image))
        (write-string "<br> " s))
      (when-let (poll (tooter:poll toot))
        (format s "Poll <ul>~%")
        (dolist (option (tooter:options poll))
          (format s "<li>~a: ~a votes</li>"
                  (tooter:title option)
                  (tooter:votes-count option)))
        (format s "</ul>~%")
)
      (dolist (a (tooter:media-attachments toot))
        (case (tooter:kind a)
          ((:image :gifv)
           (format s "<a href=\"~a\"><img src=\"~a\"></a><br>"
                   (tooter:url a)
                   (tooter:preview-url a))
           (when-let (description (tooter:description a))
             (format s "<p>~a</p><br>" description)))
          ((:video)
           (format s "<a href=\"~a\"><video controls preload=\"none\"><source src=\"~a\"></video>></a><br>"
                   (tooter:url a)
                   (tooter:preview-url a))
           (when-let (description (tooter:description a))
             (format s "<p>~a</p><br>" description)))))
      (let ((url1 (format nil "~a/@~a/~a"
                          (instance-url status)
                          (tooter:account-name (tooter:account toot))
                          (tooter:id toot)))
            (url2 (tooter:url toot)))
        (if (equal url1 url2)
            (format s "<a href=\"~a\">Open in ~a</a>"
                    url2
                    (->> toot (tooter:url) (str:split "/") (third)))
            (format s "<a href=\"~a\">Open in ~a</a> / <a href=\"~a\">in ~a</a>"
                    url1
                    (->> status
                         (instance-url)
                         (str:split "//")
                         (alexandria:lastcar))
                    url2
                    (->> toot
                         (tooter:url)
                         (str:split "/")
                         (third))))))))

;; The plain text representation serves for different purposes than
;; the HTML version, so it is not simply a flattened version without
;; markup. One use of the plain text is composing a subject line for
;; e-mails. Another use is searching the text in channel filters.
;; For this reason, the plain text version contains both content warnings
;; and image descriptions.

(defun toot-content-as-plain-text (status)
  (let ((toot (toot status)))
    (with-output-to-string (s)
      (when (tooter:sensitive toot)
        (format s "~a~%~%"
                (tooter:spoiler-text toot)))
      (dolist (paragraph (-> toot
                             (tooter:content)
                             (plump:parse)
                             (plump:get-elements-by-tag-name "p")))
        ;; Calling plump:text directly on a paragraph element removes
        ;; <br> tags, and thus separation between the lines of a
        ;; paragraph.
        (-<> paragraph
             (plump:children)
             (map 'list #'plump:text <>)
             (remove-if #'str:empty? <>)
             (str:join " " <>)
             (format s "~a~%~%" <>)))
      (dolist (a (tooter:media-attachments toot))
        (format s "~a attachment: ~a~%"
                (tooter:kind a)
                (tooter:url a))
        (when-let (description (tooter:description a))
          (format s "~%~%~a" description))))))

;; The s-expression representation of toot contents is mainly useful for
;; debugging.

(defun toot-content-as-sexp (status)
  (-> status
      (toot)
      (tooter:content)
      (plump:parse)
      (plump-sexp:serialize)))

;; The toot summary is meant to provide concise information for
;; logging or debugging.

(defun toot-summary (status)
  (with-output-to-string (s)
    (let ((toot (toot status)))
      (format s "Id: ~a~%" (tooter:id status))
      (when (boost? status)
        (format s "Boost by ~a~%Visibility: ~a~%Parent id: ~a~%"
                (tooter:account-name (tooter:account status))
                (tooter:visibility status)
                (tooter:id toot)))
      (format s "From account: ~a~%URL: ~a~%Visibility: ~a~%Text:~a ...~%Language: ~a~%"
              (tooter:account-name (tooter:account toot))
              (tooter:url toot)
              (tooter:visibility toot)
              (-<> toot
                   (toot-content-as-plain-text)
                   (str:substring 0 40 <>)
                   (str:trim)
                   (str:collapse-whitespaces))
              (tooter:language toot))
      (dolist (a (tooter:media-attachments toot))
        (format s "Attachment of kind ~a~%URL: ~a~%"
                (tooter:kind a)
                (tooter:url a))))))

;; Render the reply chain containing a toot

(defun author-display-name (status)
  (-> status
      (tooter:account)
      (tooter:display-name)))

(defun author-line (status)
  (let ((toot (toot status)))
    (with-output-to-string (s)
      (write-string (author-display-name toot) s)
      (unless (eql toot status)
        (format s " (via ~a)" (author-display-name status))))))

(defun reply-chain-as-html (status)
  (let* ((toot (toot status))
         (context (tooter:context *mastodon-client* (tooter:id toot)))
         (ancestors (tooter:ancestors context))
         (descendants (tooter:descendants context)))
    (with-output-to-string (s)
      (format s "<div>~%")
      (dolist (a ancestors)
        (format s "<p><b><i>~a</i></b></p><br>" (author-line a))
        (write-string (toot-content-as-html a) s)
        (format s "<hr width=\"20%\" size=\"5px\" color=\"gray\" align=\"left\"/>~%"))
      (format s "</div>~%<div><p><b><i>~a</i></b></p><br>~a</div>~%"
              (author-line toot)
              (toot-content-as-html toot))
      (format s "<div>~%")
      (dolist (d descendants)
        (format s "<hr width=\"20%\" size=\"5px\" color=\"gray\" align=\"right\"/>~%")
        (format s "<p><b><i>~a</i></b></p><br>" (author-line d))
        (write-string (toot-content-as-html d) s)
        (format s "~%"))
      (format s "</div>~%"))))
