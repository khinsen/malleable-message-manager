;;;; Q-encoding for non-ASCII text in mail headers

;; This code is inspired by, and uses functions from, the
;; package cl-qprint, which performs quoted-printable
;; encoding as used in the mail body. The main difference
;; between the two encodings is that Q-encoding adds a prefix
;; indicating the character set. Since this prefix uses the
;; question mark, the ASCII character #\? must be encoded
;; as well if it occurs in the text.

(in-package #:malleable-message-manager)

(defparameter *ascii-?* (cl-qprint::char-code-ascii #\?)
  "ASCII code for question mark character.")

(defun q-encode-byte-p (byte)
  (or (< byte cl-qprint::*ascii-!*)
      (> byte cl-qprint::*ascii-~*)
      (= byte cl-qprint::*ascii-=*)
      (= byte *ascii-?*)))

(defun q-encode-byte (byte)
  "Encode BYTE for quoted-printable encoding and return string."
  (if (q-encode-byte-p byte)
      (cl-qprint::byte-encoding byte)
      (make-string 1 :initial-element (cl-qprint::code-char-ascii byte))))

(defun q-encode (input charset)
  (let ((in (etypecase input
	      (vector (flexi-streams:make-in-memory-input-stream input))
	      (stream input))))
    (with-output-to-string (out)
      (write-string "=?" out)
      (write-string (string charset) out)
      (write-string "?Q?" out)
      (loop for byte = (read-byte in nil 'eof)
         while (not (eq byte 'eof)) do
           (write-string (q-encode-byte byte) out))
      (write-string "?=" out))))

(defun q-encode-if-8bit (input charset)
  ;; input must be an octet vector
  (if (some (lambda (byte) (> byte cl-qprint::*ascii-~*)) input)
      (q-encode input charset)
      (babel:octets-to-string input :encoding charset)))

(defun q-encode-header-line (line)
  (q-encode-if-8bit
   (babel:string-to-octets line :encoding :utf-8)
   :utf-8))

(defun decode-header-line (s)
  (-> s
      (decode-substrings "=?utf-8?q?" #'cl-qprint:decode)
      (decode-substrings "=?utf-8?b?" #'base64:base64-string-to-usb8-array)))

(defun decode-substrings (s prefix decoder-fn)
  (if-let (start (search prefix (str:downcase s)))
    (if-let (end (search "?=" s :start2 start))
      (let ((decoded (-<> s
                          (str:substring (+ start 10) end <>)
                          (funcall decoder-fn <>)
                          (babel:octets-to-string <> :encoding :utf-8))))
        (str:concat decoded
                    (decode-substrings (str:substring (+ end 2) nil s)
                                       prefix decoder-fn)))
      s)
    s))
