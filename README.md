# Malleable Message Manager

A Common Lisp library for working with messages on Mastodon, e-mails, and RSS feeds.

## What can you do with it?

See the `examples` directory. For now, it contains four examples:

- A Mastodon client that dispatches toots from a user's home timeline to different channels, a channel being a destination (a mailbox folder or an RSS feed) plus a filter that selects toots according to various criteria.

- A script that copies conversations (toots with replies, replies to replies, etc.) to a mail folder, skipping toots that are already present. A nice way to stay up to date with lengthy and branching conversations.

- A short script for exploring the structure and contents of a single toot.

- A script that takes bookmarks in a mail folder, and lists them
  sorted by date in an HTML file. Each message in the folder holds one
  bookmark, with the title in the subject line and the URL in the body
  text. The directory `tools/termux` contains two scripts that create 
  bookmarks in this format on an Android device via
  [Termux](https://termux.dev/)
  (see [these instructions](https://wiki.termux.com/wiki/Intents_and_Hooks
  for how to use `termux-url-opener`).

## Malleable software

This library is intended to be [malleable software](https://malleable.systems/), which means that you should not expect to use it as-is, but as a starting point for your own version, adapted to your needs. In that spirit, there is no API documentation. You are supposed to read the code. However, I try to make the code as readable and as simple as possible, avoiding bureaucratic overhead such as configuration files (everything is Lisp code).

Malleable software also means that there is no maintainer. We are all equals. If you find a bug, or have a great idea for improving the code, I'd love to hear from you, but I make no promises. In particular, I will accept bug fix contributions, but not feature requests, even if they come with working code. Instead, keep your new feature in your own repository and tell the world about it. Whoever finds it useful can then integrate it into their own version. No code bloat!

## Why Common Lisp?

1. It's [boring technology](https://boringtechnology.club/),
   with a very low level of technology churn.
2. It runs on all devices and platforms that I use.
3. It has a good library ecosystem that I can build on (see file `malleable-message-manager.asd` for dependencies).
4. It supports live development, which I happen to like.

## License

This library is Free Software, licensed under the [GNU General Public License version 3](https://www.gnu.org/licenses/gpl-3.0.en.html).


