;;;; Extracting information from statuses and toots

(in-package #:malleable-message-manager)

;; Not every status is a toot. Boosts and notifications, for example, are
;; statuses, but not toots. They *refer* to toots. The toot itself is
;; accessible as (tooter:parent status).

(defun boost? (status)
  (and (tooter:parent status)
       t))

(defun toot (status)
  (if-let (parent (tooter:parent status))
    parent
    status))

;; Toot IDs are specific to an instance. (tooter:id ...) is the ID on
;; the instance from which the toot was retrieved. In some contexts,
;; the ID on the originating instance is more useful, because it is
;; globally unique.  It can be retrieved from the toot's URL.

(defun id-on-origin-instance (status)
  (->> status
       toot
       (tooter:url)
       (str:split "/")
       (alexandria:lastcar)))

;; For accounts on the instance being used, tooter:account-name returns
;; just the user name, without the instance. For universally valid account
;; names, the instance must be added.

(defun full-account-name (account)
  (let ((name (tooter:account-name account)))
    (if (str:contains? "@" name)
        name
        (str:concat name "@"
                    (->> account
                         (tooter:url)
                         (str:split "/")
                         (third))))))

;; There are two mechanisms for exploring reply trees: the "in-reply-to-id"
;; field of a toot, and its context, accessible via tooter:context. The context
;; is the only way to find descendants, i.e. replies to a toot and replies
;; to replies. However, the context is less reliable than the "in-reply-to-id"
;; field for finding ancestors.

(defun reply-chain (toots)
  (if-let (parent-id (tooter:in-reply-to-id (car toots)))
    (reply-chain (cons (tooter:find-status *mastodon-client* parent-id) toots))
    toots))

(defun root-of-reply-tree (toot)
  (reply-chain (list toot)))

;; A reply is just a list of the toots in that tree. The reply-to relations
;; between toots can be obtained form the status objects.

(defun reply-tree (toot)
  (let* ((root (first (root-of-reply-tree toot)))
         (context (tooter:context *mastodon-client* (tooter:id root)))
         (ancestors (tooter:ancestors context))
         (descendants (tooter:descendants context)))
    (assert (null ancestors))
    (cons root descendants)))

;; Attachments are stored on the originating instance, and/or on
;; a Mastodon-specific content-delivery network. The toots contain
;; only URLs. The following function downloads an attachment to a
;; supplied output stream.

(defun download-url-to-stream (url output)
  (let* ((response (multiple-value-list (drakma:http-request url :want-stream t)))
         (stream (first response))
         (code (second response)))
    (unless (= 200 code)
      (error "HTTP error ~s" code))
    (loop for byte = (read-byte stream nil)
          while byte
          do (write-byte byte output))
    (close stream)))
