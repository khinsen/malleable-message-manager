;;;; Transform toot to an item in an RSS feed

(in-package #:malleable-message-manager)

;; The title is optional for an RSS item. It's a good place for
;; content warnings.

(defun rss-title (status)
  (if (tooter:sensitive status)
      (format nil "~a (~a)"
              (tooter:spoiler-text status)
              (rss-author-short status))
      (rss-author-short status)))

(defun rss-author-short (status)
  (if (boost? status)
    (format nil "~a (via ~a)"
            (author-short (toot status))
            (author-short status))
    (author-short status)))

(defun author-short (status)
  (-> status
      tooter:account
      full-account-name))

;; RSS recommends using the format "e-mail (name)", which we
;; adapt by substituting the account name for the e-mail.

(defun format-author-for-rss (status)
  (let ((account (tooter:account status)))
    (format nil "~a (~a)"
            (full-account-name account)
            (tooter:display-name account))))

;; For boosts, add the boosting account in parentheses

(defun rss-author (status)
  (if (boost? status)
    (format nil "~a (via ~a)"
            (format-author-for-rss (toot status))
            (format-author-for-rss status))
    (format-author-for-rss status)))

;; RSS wants time stamps in RFC 1123 format.

(defun rss-date (status)
  (-<> (tooter:created-at status)
       (local-time:universal-to-timestamp)
       (local-time:format-timestring nil <>
          :format local-time:+rfc-1123-format+)))

;; The link is always the one for the original toot, since
;; boosts don't have URLs.

(defun rss-link (status)
  (-> status
      (toot)
      (tooter:url)))

;; The guid is the URL.

(defun rss-guid (status)
  (rss-link status))

;; The description is just the content of the toot.

(defun rss-description (status)
  (toot-content-as-html status))

;; Return an plump node representing the toot as an RSS
;; item. Non-literal strings are coerced to values of type
;; simple-string. Plump assumes simple-string in serialization for
;; performance reasons.

(defun toot-as-rss-item (status)
  (let* ((root (plump:make-root))
         (item (plump:make-element root "item")))
    (when-let (title (rss-title status))
      (-<> (plump:make-element item "title")
           (plump:make-text-node <> (coerce title 'simple-string))))
    (-<> (plump:make-element item "link")
         (plump:make-text-node <> (coerce (rss-link status) 'simple-string)))
    (-<> (plump:make-element item "guid")
         (plump:make-text-node <> (coerce (rss-guid status) 'simple-string)))
    (-<> (plump:make-element item "author")
         (plump:make-text-node <> (coerce (rss-author status) 'simple-string)))
    (-<> (plump:make-element item "pubDate")
         (plump:make-text-node <> (coerce (rss-date status) 'simple-string)))
    (-<> (plump:make-element item "description")
         (plump:make-text-node <> (coerce (rss-description status) 'simple-string)))
    item))
