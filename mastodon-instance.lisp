;;;; Configuration data:
;;;;  - URL of the Mastodon instance to be used
;;;;  - Account name as used in toots
;;;;  - Initialized client

(in-package #:malleable-message-manager)

;; Give appropriate values to these variables using setf
;; before using any Mastodon-related functions.

;; No trailing slash
(defvar *mastodon-instance-url* "https://example.com")

;; No initial @
(defvar *mastodon-account-name* "me@example.com")

;; See https://shinmera.github.io/tooter/ on how to obtain
;; all the keyword arguments for creating a client.
(defvar *mastodon-client*
  (make-instance 'tooter:client
                 :base *mastodon-instance-url*
                 :name "..."
                 :key "..."
                 :secret "..."
                 :access-token "..."))
