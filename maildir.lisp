;;;; Configuration data:
;;;;  - Base directory of the Maildir hierarchy

(in-package #:malleable-message-manager)

(defvar *maildir-directory*
  (merge-pathnames #P"Mail/" (user-homedir-pathname)))
