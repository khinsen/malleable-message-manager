;;;; Functions that check properties of toots, for use in channel filters

(in-package #:malleable-message-manager)

;; All these functions return either nil or t, but not arbitrary
;; truthy values. This is implemented using the (and ... t) idiom and
;; avoids leaking implementation details.

;; Test toot visibility. I don't put private or direct toots into RSS
;; feeds, because this would render them publicly accessible.

(defun public? (status)
  (equal (tooter:visibility (toot status)) :public))

(defun unlisted? (status)
  (equal (tooter:visibility (toot status)) :unlisted))

(defun private? (status)
  (equal (tooter:visibility (toot status)) :private))

(defun direct? (status)
  (equal (tooter:visibility (toot status)) :direct))

;; Toots that mention me get special treatment to make sure I don't
;; miss them.

(defun my-account-url ()
  (-> *mastodon-client*
      (tooter:account)
      (tooter:url)))

(defun my-account-id ()
  (-> *mastodon-client*
      (tooter:account)
      (tooter:id)))

(defun mentions-me? (status)
  (and (member (my-account-url)
               (mapcar #'tooter:url (tooter:mentions status))
               :test #'string=)
       t))

(defun reply-to-me? (status)
  (and (equal (my-account-id)
              (tooter:in-reply-to-account-id status))
       t))

(defun boosted-by-me? (status)
  (and (boost? status)
       (equal (my-account-id)
              (tooter:account-name (tooter:account status)))
       t))

;; The most frequently used criterion in a channel filter is the
;; account that has sent a toot.

(defun account-in? (account-list)
  (lambda (status)
    (and (member (tooter:account-name (tooter:account (toot status)))
                 account-list
                 :test #'equal)
         t)))

;; Tags are another very useful filter criterion.

(defun tag-in? (tag-list)
  (lambda (status)
    (and (intersection (->> status
                            (tooter:tags)
                            (mapcar #'tooter:name)
                            (mapcar #'str:downcase))
                       tag-list
                       :test #'equal)
         t)))

;; A toot looks Greek to you? Filter it out!

(defun language-in? (language-list)
  (lambda (status)
    (and (member (tooter:language (toot status))
                 language-list
                 :test #'equal)
         t)))


;; Scan the plain text version of the toot for occurrences of
;; a list of substrings. The comparison is not case sensitive.

(defun contains-substrings? (substrings)
  (let ((substrings-lower-case (mapcar #'str:downcase substrings)))
    (lambda (status)
      (let ((text (-> status
                      (toot-content-as-plain-text)
                      (str:downcase))))
        (and (some #'(lambda (ss) (str:contains? ss text))
                   substrings-lower-case)
             t)))))

;; Checking if a toot is in a registry allows filtering out multiple
;; boosts of the same toot.

(defun in-registry? (registry)
  (lambda (status)
    (and (registration-timestamp status registry)
         t)))

;; Or/and combinators for filters.

(defun one-of (&rest fns)
  (lambda (status)
    (some #'(lambda (fn)
              (let ((test (funcall fn status)))
                (vom:debug3 "Subtest ~a: ~A" test fn)
                test))
          fns)))

(defun all-of (&rest fns)
  (lambda (status)
    (every #'(lambda (fn)
              (let ((test (funcall fn status)))
                (vom:debug3 "Subtest ~a: ~A" test fn)
                test))
          fns)))
