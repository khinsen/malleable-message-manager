;;;; Counters

(in-package #:malleable-message-manager)

;; One selection criterion for accounts or topics is their frequency in a timeline.
;; The counters defined here keep track of events occurring in a timeline,
;; within a time window of fixed length.

(defclass event-counter ()
  ((attribute-date-hashmap :reader attribute-date-hashmap :initform (dict))
   (window :reader window :initarg :window)))

(defun make-event-counter (amount unit)
  (make-instance 'event-counter :window (list amount unit)))

;; Register an event, and clean up its attribute's history by removing events
;; outside of the time window.

(defun register-event (attribute counter)
  (let ((now (local-time:now))
        (events (gethash attribute (attribute-date-hashmap counter) nil)))
    (setf (gethash attribute (attribute-date-hashmap counter))
          (cons now events))))

;; Look up the event count within the time window for an
;; attribute. Remove older entries at this occasion, to prevent the
;; counters from growing in size over time.

(defun event-count (attribute counter)
  (let* ((now (local-time:now))
         (limit (apply #'local-time:timestamp- (cons now (window counter))))
         (events (gethash attribute (attribute-date-hashmap counter) nil))
         (recent-events (remove-if #'(lambda (e) (local-time:timestamp< e limit)) events)))
    (setf (gethash attribute (attribute-date-hashmap counter)) recent-events)
    (length recent-events)))
