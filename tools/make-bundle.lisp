;; Make a Quicklisp bundle for malleable-message-manager.
;;
;; Please read the documentation first:
;;    https://www.quicklisp.org/beta/bundles.html

;; Particularity: In the current Quicklisp distribution (as of 2023-09-11),
;; tooter doesn't work (see https://github.com/Shinmera/tooter/issues/42.
;; The following list of systems includes the dependencies of both
;; tooter and malleable-message-manager. You have to add the source code
;; of both to local-projects.

(ql:bundle-systems
 (list
   ;; dependencies of malleable-message-manager
  :alexandria
  :arrow-macros
  :babel
  :cl-mime
  :cl-qprint
  :flexi-streams
  :local-time
  :mel-base
  :plump
  :plump-sexp
  :serapeum
  :str
  :vom
  ;; dependencies of tooter not yet listed above
  :yason
  :cl-ppcre
  :drakma
  :documentation-utils
  ;; dependencies of html-inspector-views not yet listed above
  :cl-who
  :cl-base32
  ;; additional dependencies for my client scripts
  :cl-store
  :trivial-shell)
 :to "~/common-lisp-bundles/malleable-message-manager/")
