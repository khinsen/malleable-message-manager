;;;; Package definition

(defpackage :malleable-message-manager
  (:nicknames :mmm :net.khinsen.malleable-message-manager)
  (:use :cl)
  ;; Local nicknames aren't part of the standard, but widely supported
  ;; and... convenient.
  (:local-nicknames (:hiv :html-inspector-views))
  ;; A few frequently used symbols are imported, as I consider them
  ;; part of my general Lisp vocabulary.
  (:import-from :arrow-macros
   :-> :-<> :->> :-<>> :<> :some-> :some->>)
  (:import-from :alexandria
   :if-let :when-let)
  (:import-from :serapeum
    :dict :hash-fold :and-let*)
  ;; The export list contains the symbols that I consider most useful
  ;; in client scripts and apps that (:use :mmm) for convenience.
  (:export
   :-> :-<> :->> :-<>> :<>  ;; from arrow-macros
   :some-> :some->>         ;; from arrow-macros
   :if-let :when-let        ;; from alexandria
   :dict :hash-fold         ;; from serapeum
   ;; mastodon-instance.lisp
   :*mastodon-instance-url*
   :*mastodon-account-name*
   :*mastodon-client*
   ;; maildir.lisp
   :*maildir-directory*
   ;; mime-types.lisp
   :mime-type
   ;; toots.lisp
   :toot
   :boost?
   :id-on-origin-instance
   :full-account-name
   :reply-tree
   :download-url-to-stream
   ;; foreign-toots.lisp
   :retrieve-from-origin-instance
   :reply-tree-from-origin-instances
   :find-status-from-url
   ;; toot-formatting.lisp
   :toot-content-as-html
   :toot-content-as-plain-text
   :toot-content-as-sexp
   :toot-summary
   :reply-chain-as-html
   ;; toot-properties.lisp
   :public?
   :unlisted?
   :private?
   :direct?
   :mentions-me?
   :reply-to-me?
   :boosted-by-me?
   :account-in?
   :tag-in?
   :language-in?
   :contains-substrings?
   :in-registry?
   :one-of :all-of
   ;; toot-to-mail.lisp
   :toot-as-message
   :store-message-in-folder
   ;; toot-to-rss-item.lisp
   :toot-as-rss-item
   ;; channels.lisp
   :make-message-channel
   :maybe-put
   :flush
   :filename
   ;; dispatchers.lisp
   :toot-dispatcher :channels
   :make-toot-dispatcher
   :dispatch :flush
   ;; rss-feeds.lisp
   :make-rss-channel
   :make-mail-channel
   :make-rss-feed
   ;; toot-registries.lisp
   :make-toot-registry
   :register
   :registration-timestamp
   :remove-before
   :max-id
   ;; counters.lisp
   :make-event-counter
   :register-event
   :event-count
   ;; timelines.lisp
   :timeline-from-id
   ;; bookmarks.lisp
   :title-and-url))
