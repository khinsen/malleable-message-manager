;;;; Channels into which messages are dispatched

(in-package #:malleable-message-manager)

;; Message channels

(defclass message-channel ()
  ((filter :reader filter :initarg :filter)))

(defun make-message-channel (filter)
  (make-instance 'message-channel :filter filter))

(defgeneric put (toot channel)
  (:documentation "Put a toot into the channel."))

(defgeneric flush (channel)
  (:documentation "Process the toots remaining in the channel."))

(defmethod flush ((channel message-channel))
  (declare (ignore channel)))

(defgeneric filename (channel)
  (:documentation "Name of the file storing the channel's toots, or nil."))

(defmethod filename ((channel message-channel))
  (declare (ignore channel))
  nil)

(defun maybe-put (toot channel)
  (vom:debug2 "Considering channel ~s for toot ~a~%"
              channel
              (tooter:id toot))
  (let* ((fn (filter channel))
         (test (funcall fn toot)))
    (vom:debug3 "Test ~a: ~A" test fn)
    (when test
      (put toot channel))
    test))

;; RSS channels

;; Messages put into an RSS channel are stored in a list. When the
;; channel is flushed, the prior version of its feed file is read in,
;; the toots from the channel are added, and the feed is written to
;; the file, overwriting the prior version. In order to prevent the
;; file from growing in size forever, toots from the prior version
;; are deleted if the feed size after flushing the channel exceeds
;; a given limit.

(defclass rss-channel (message-channel)
  ((filename :reader rss-filename :initarg :filename)
   (max-size :reader max-size :initarg :max-size)
   (toots :accessor toots :initform nil)))

(defun make-rss-channel (filename filter &key (max-size 100))
  (make-instance 'rss-channel
                 :filename filename
                 :max-size max-size
                 :filter filter))

(defmethod print-object ((channel rss-channel) stream)
  (print-unreadable-object (channel stream :type T)
    (format stream "~a, ~d toots"
            (pathname-name (filename channel))
            (length (toots channel)))))

(defmethod put (toot (channel rss-channel))
  (push toot (toots channel)))

(defmethod flush ((channel rss-channel))
  (let* ((feed (make-rss-feed (rss-filename channel)))
         (channel-element (-<> feed
                               (plump:first-element)
                               (plump:first-element)))
         (items (-<> channel-element
                     (plump:get-elements-by-tag-name <> "item")) )
         (delete-count (min (- (+ (length (toots channel))
                                  (length items))
                               (max-size channel))
                            (length items))))
    (when (> delete-count 0)
      (dolist (item (subseq items 0 delete-count))
        (plump:remove-child item)))
    (dolist (toot (toots channel))
      (plump:append-child channel-element (toot-as-rss-item toot)))
    (with-open-file (output (rss-filename channel)
                            :direction :output
                            :if-exists :supersede)
      (plump:serialize feed output))
    (setf (toots channel) nil)))

(defmethod filename ((channel rss-channel))
  (rss-filename channel))

(defmethod hiv:text-representation ((channel rss-channel))
  (format nil "RSS feed ~a" (rss-filename channel)))

;; Mail channels

;; Each message put into a mail channel is converted into a MIME
;; message and stored in a mail folder. Nothing is stored and there is
;; no need to flush the channel in the end.

(defclass mail-channel (message-channel)
  ((mail-folder :accessor mail-folder :initarg :mail-folder)))

(defun make-mail-channel (mail-folder filter)
  (make-instance 'mail-channel :mail-folder mail-folder :filter filter))

(defmethod print-object ((channel mail-channel) stream)
  (print-unreadable-object (channel stream :type T)
    (format stream "~a" (mel:short-name (mail-folder channel)))))

(defmethod put (toot (channel mail-channel))
  (let ((folder (mail-folder channel))
        (toot-message (toot-as-message toot)))
    (store-message-in-folder toot-message folder)))

(defmethod hiv:text-representation ((channel mail-channel))
  (str:concat "Mailbox " (-> channel mail-folder hiv:text-representation )))
