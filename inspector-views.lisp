;;;; Views for clog-inspector

(in-package #:malleable-message-manager)

;; tooter:client

(defmethod hiv:text-representation ((client tooter:client))
  (str:concat "Mastodon client for "
              (-> client tooter:account full-account-name)))

(hiv:defview 👀account (client tooter:client)
  (-> client
      tooter:account
      hiv:👀description
      (hiv:rename  :title "Account"
                   :priority 1)))

(hiv:defview 👀timeline (client tooter:client)
  (-> client
      (tooter:timeline :home)
      hiv:👀items
      (hiv:rename :title "Timeline"
                  :priority 2)))

(hiv:defview 👀bookmarks (client tooter:client)
  (-> (tooter:collect-all-pages client (tooter:bookmarks client))
      hiv:👀items
      (hiv:rename :title "Bookmarks"
                  :priority 3)))

(defmethod hiv:open-package-operations? ((client tooter:client) package-name)
  (member package-name '("TOOTER-CLIENT" "TOOTER-OBJECTS" "TOOTER-QUERIES")
          :test #'string=))

;; tooter:status

(defmethod hiv:text-representation ((status tooter:status))
  (format nil "~a: ~a"
          (-> status tooter:account full-account-name)
          (->> status toot-content-as-plain-text (str:shorten 40))))

(hiv:defview 👀content (status tooter:status)
  (hiv:html-view :title "Content" :priority 1
    (hiv:html
      (:div (:b (hiv:esc "Author: "))
            (hiv:esc (format-author-for-rss (toot status)))
            (when (boost? status)
              (hiv:html
                (:br)
                (:b (hiv:esc "Boosted by: "))
                (hiv:esc (format-author-for-rss status)))))
      (:div (hiv:str (toot-content-as-html status))))))

(hiv:defview 👀mentions (status tooter:status)
  (-> status
      tooter:mentions
      hiv:👀items 
      (hiv:rename :title "Mentions"
                  :priority 3)))

(hiv:defview 👀account (status tooter:status)
  (-> status
      tooter:account
      hiv:👀description
      (hiv:rename :title "Account"
                  :priority 5)))

(defmethod hiv:open-package-operations? ((status tooter:status) package-name)
  (member package-name '("TOOTER" "TOOTER-OBJECTS")
          :test #'string=))

;; tooter:account

(defmethod hiv:text-representation ((account tooter:account))
  (str:concat "Account " (-> account full-account-name)))

(hiv:defview hiv:👀description (account tooter:account)
  (hiv:html-view :title "Description" :priority 1
    (hiv:html
      (:a :href (tooter:url account)
          :target "_blank"
          (hiv:esc (-> account full-account-name)))
      (:br) (:br)
      (:table
       (hiv:html
         (dolist (field (tooter:fields account))
           (hiv:html
             (:tr (:td (hiv:str (tooter:name field)))
                  (:td (hiv:str (tooter::value field))))))))
      (:br) (:br)
      (hiv:str (tooter:note account)))))

;; tooter:mention

(defmethod hiv:text-representation ((mention tooter:mention))
  (str:concat "Mention " (-> mention tooter:account-name)))

;; tooter:poll and tooter:poll-option

(defmethod hiv:text-representation ((poll tooter:poll))
  (format nil "Poll with ~a votes"
          (-> poll tooter:votes-count)))

(defmethod hiv:text-representation ((option tooter:poll-option))
  (format nil "Poll option \"~a\" with ~a votes"
          (-> option tooter:title)
          (-> option tooter:votes-count)))

(hiv:defview 👀options (poll tooter:poll)
  (-> poll tooter:options hiv:👀items
      (hiv:rename :title "Options"
                  :priority 1)))

;; mel:folder

(defmethod hiv:text-representation ((folder mel:folder))
  (mel:short-name folder))

(hiv:defview hiv:👀items (folder mel:folder)
  (hiv:multi-column-list-view
   (mel.public:messages folder)
   :title "Items"
   :priority 0
   :columns '("Date" "From" "Subject")
   :display (list #'message-date
                  #'(lambda (m) (-> m mel:from decode-address))
                  #'(lambda (m) (-> m mel:subject decode-header-line)))))

;; mel:mime-message

(defmethod hiv:text-representation ((message mel:mime-message))
  (mel:uid message))

(hiv:defview 👀message (message mel:mime-message)
  (hiv:html-view :title "Message" :priority 0
    (hiv:html
      (:table :class "inspector-table"
              (:tr (:td (:b "Date"))
                   (:td (hiv:object-ref (-> message message-date))))
              (:tr (:td (:b "From"))
                   (:td (hiv:object-ref (-> message mel:from)
                                        :display #'decode-address)))
              (:tr (:td (:b "To"))
                   (:td (hiv:object-ref (-> message mel:to)
                                        :display #'decode-address-list)))
              (when-let (cc (-> message mel:cc-list))
                (hiv:html
                  (:tr (:td (:b "CC"))
                       (:td (hiv:object-ref cc
                                            :display #'decode-address-list)))))
              (:tr (:td (:b "Subject"))
                   (:td (hiv:object-ref (-> message mel:subject)
                                        :display #'decode-header-line))))
      (:br)
      (:div (hiv:esc (-> message
                         mel:message-body-stream
                         alexandria:read-stream-content-into-string))))))

(defun message-date (message)
  (-> message
      mel:date
      mel:universal-time-to-date))

(defun decode-address-list (addresses)
  (->> addresses
       (mapcar #'decode-address)
       (format nil "~{~a~^, ~}")))

(defun decode-address (address)
  (if-let (display (decode-display-name address))
    (str:concat display
                " <"
                (mel:address-spec address)
                ">")
    (mel:address-spec address)))

(defun decode-display-name (address)
  (when (slot-boundp address 'mel:display-name)
      (-> address
          mel:display-name
          str:trim
          decode-header-line)))

(hiv:defview 👀parts (message mel:mime-message)
  (when-let (parts (mel:parts message))
    (-> parts
        hiv:👀items
        (hiv:rename :title "Parts"
                    :priority 2))))

(defmethod hiv:open-package-operations? ((message mel:mime-message) package-name)
  (member package-name '("MEL.PUBLIC" "MEL.MIME")
          :test #'string=))

;; mel:part, mel:multipart

(defmethod hiv:text-representation ((part mel:part))
  (multiple-value-bind (supertype subtype parameters)
      (mel:content-type part)
    (format nil "~a ~d: ~a/~a ~{~a=\"~a\" ~}"
            (-> part class-of class-name str:downcase)
            (-> part (slot-value 'mel.mime::part-number))
            (-> supertype str:downcase)
            (-> subtype str:downcase)
            parameters)))

(hiv:defview 👀parts (part mel:multipart)
  (when-let (parts (mel:parts part))
    (-> parts
        hiv:👀items
        (hiv:rename :title "Parts"
                    :priority 2))))

(hiv:defview 👀content (part mel:part)
  (hiv:html-view :title "Content" :priority 5
    (content-for-type (-> part mel:content-type)
                      part)))

(hiv:defview 👀content (part mel:multipart)
  nil)

(defun content-supertype (part)
  (mel:content-type part))

(defun content-subtype (part)
  (multiple-value-bind (supertype subtype parameters)
      (mel:content-type part)
    (declare (ignore supertype))
    (declare (ignore parameters))
    subtype))

(defun content-type (part)
  (multiple-value-bind (supertype subtype parameters)
      (mel:content-type part)
    (declare (ignore parameters))
    (format nil "~a/~a" supertype subtype)))

(defgeneric content-for-type (mime-supertype part)
  (:method ((type t) (part mel:part))
    nil))

(defmethod content-for-type ((type (eql :text)) (part mel:part))
  (let ((string (-> part mel:part-body-string)))
    (cond
      ((equal :html (content-subtype part))
       (hiv:html (hiv:str string)))
      (t (hiv:html (:pre (hiv:esc string)))))))

(defmethod content-for-type ((type (eql :image)) (part mel:part))
  (hiv:html
    (hiv:fmt "<img src='data:~a;~a,~a'/>"
             (-> part content-type)
             (-> part mel:content-transfer-encoding str:downcase)
             (-> part mel:part-body-string))))

(defmethod hiv:open-package-operations? ((part mel:part) package-name)
  (member package-name '("MEL.PUBLIC" "MEL.MIME")
          :test #'string=))
