;;;; Bookmarks stored in mail messages

(in-package #:malleable-message-manager)

;; Extract bookmark title and URL from a bookmark message
;; The format is very simple: title in the subject line,
;; URL in the sole line of the body.
;; Some bookmarks have no title, show the URL instead
;; to have something to click on.

(defun title-and-url (message)
  (let ((url (-> message
                 mel:message-body-stream
                 alexandria:read-stream-content-into-string
                 str:trim))
        (title (->> message
                    mel:subject
                    decode-header-line
                    str:trim)))
    (values
     (if (zerop (length title)) url title)
     url)))
