(ql:quickload :malleable-message-manager)
(ql:quickload :cl-store)

(defpackage :social-graph
  (:use :cl :malleable-message-manager))

(in-package :social-graph)

;; Configuration for your Mastodon instance
;;
;; Set *mastodon-instance-url*, *mastodon-account-name*, and
;; *mastodon-client*. See file mastodon-instance.lisp for details.

(load
 (merge-pathnames #P".config/malleable-message-manager/mastodon-instance.lisp"
                  (user-homedir-pathname)))

;; Retrieve the accounts with following, follower, or mute relation

(defvar *following*
  (tooter:collect-all-pages *mastodon-client*
                            (tooter:get-following *mastodon-client* t)))

(defvar *followers*
  (tooter:collect-all-pages *mastodon-client*
                            (tooter:get-followers *mastodon-client* t)))

(defvar *mutes*
  (tooter:collect-all-pages *mastodon-client*
                            (tooter:mutes *mastodon-client*)))

;; Find mutuals

(defvar *mutuals*
  (intersection *following* *followers* :key #'tooter:account-name :test #'equal))

;; Did I mute any mutuals (by accident, or temporarily)?

(defvar *muted-mutuals*
  (intersection *mutuals* *mutes* :key #'tooter:account-name :test #'equal))
