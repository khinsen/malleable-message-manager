(ql:quickload :malleable-message-manager)

(defpackage :mastodon-exploration
  (:use :cl :malleable-message-manager))

(in-package :mastodon-exploration)

;; Configuration for your Mastodon instance
;;
;; Sets *mastodon-instance-url*, *mastodon-account-name*, and
;; *mastodon-client*. See file mastodon-instance.lisp for details.

(load
 (merge-pathnames #P".config/malleable-message-manager/mastodon-instance.lisp"
                  (user-homedir-pathname)))

;; Retrieve a single toot via its ID (the last part of the URL when
;; you look at the toot in the Web app).

(defvar *toot* (tooter:find-status *mastodon-client* "111187352748966167"))

;; Some expressions to evaluate for exploring the toot.
;; Remember that the SLIME (or Sly) inspector is your best friend!

(toot-summary *toot*)

(toot-content-as-plain-text *toot*)

(toot-content-as-sexp *toot*)

(tooter:media-attachments *toot*)

;; Get the toot from its origin instance

(defvar *origin-client*
  (make-instance 'tooter:client
                 :base (format nil "https://~a"
                               (->> *toot*
                                    (tooter:url)
                                    (str:split "/")
                                    (third)))
                 :name "mmm"))

(defvar *origin-toot*
  (tooter:find-status *origin-client*
                      (->> *toot*
                           (tooter:url)
                           (str:split "/")
                           (alexandria:lastcar))))
