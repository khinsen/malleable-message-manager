(ql:quickload :malleable-message-manager)
(ql:quickload :trivial-open-browser)

;; This is a development tool for playing with toot rendering.

(defpackage :mastodon-toot-viewer
  (:use :cl :malleable-message-manager))

(in-package :mastodon-toot-viewer)

;; Configuration for your Mastodon instance
;;
;; Sets *mastodon-instance-url*, *mastodon-account-name*, and
;; *mastodon-client*. See file mastodon-instance.lisp for details.

(load
 (merge-pathnames #P".config/malleable-message-manager/mastodon-instance.lisp"
                  (user-homedir-pathname)))

;; Retrieve a single toot via its ID (the last part of the URL when
;; you look at the toot in the Web app).

(defvar *toot* (tooter:find-status *mastodon-client* "111108917806686771"))

;; Create an HTML file with the representation of a toot.

(defvar *html-filename*
  (merge-pathnames #P"Temp/toot.html"
                   (user-homedir-pathname)))

(defvar *html-template*
  "<!doctype html>
<html lang=\"en-US\">
  <head>
    <meta charset=\"UTF-8\" />
    <title>Toot rendering test</title>
  </head>
  <body>
    <div> ~a </div>
    <hr>
    <div> ~a </div>
  </body>
</html>
")

;; Write the HTML file and open it in the default browser.

(with-open-file (output *html-filename*
                        :direction :output
                        :if-exists :supersede)
  (format output *html-template*
          (toot-content-as-html *toot*)
          (reply-chain-as-html *toot*)))

(trivial-open-browser:open-browser
 (format nil "file://~a" *html-filename*) )
