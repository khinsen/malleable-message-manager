(ql:quickload :malleable-message-manager)
(ql:quickload :cl-store)

(defpackage :mastodon-client
  (:use :cl :malleable-message-manager))

(in-package :mastodon-client)

;; Logging level
(vom:config t :info)

;; Configuration for your Mastodon instance
;;
;; Set *mastodon-instance-url*, *mastodon-account-name*, and
;; *mastodon-client*. See file mastodon-instance.lisp for details.

(load
 (merge-pathnames #P".config/malleable-message-manager/mastodon-instance.lisp"
                  (user-homedir-pathname)))

;; Set *maildir-directory*. See maildir.lisp for details

(load
 (merge-pathnames #P".config/malleable-message-manager/maildir.lisp"
                  (user-homedir-pathname)))

;; The client keeps a registry that stores the toots seen in the last
;; seven days. The registry is made persistent by storing it in a
;; file. It serves two purposes: (1) to filter out boosts of toots
;; that have already been treated and (2) to keep track of the
;; last-seen toot, for continuing in the right place on the timeline
;; at the next invocation.

(defvar *persistent-state-directory*
  (merge-pathnames #P".cache/mmm/" (user-homedir-pathname)))

(defvar *registry-file*
  (merge-pathnames #P"toot-registry" *persistent-state-directory*))

;; If the registry file exists, it is read, otherwise an empty registy
;; is created.

(defvar *registry*
  (with-open-file (input *registry-file*
                         :direction :input
                         :element-type '(unsigned-byte 8)
                         :if-does-not-exist nil)
    (or (and input
             (cl-store:restore input))
        (make-toot-registry))))

;; The client also keeps statistics on accounts and keywords occuring in the
;; timeline. This permits picking out rare events and prevent them from
;; being drowned in high-frequency events.

(defvar *counter-file*
  (merge-pathnames #P"event-counters" *persistent-state-directory*))

(defvar *counter*
  (with-open-file (input *counter-file*
                         :direction :input
                         :element-type '(unsigned-byte 8)
                         :if-does-not-exist nil)
    (or (and input
             (cl-store:restore input))
        (make-event-counter 7 :day))))

;; Two macros make channel definitione more readable

(defmacro mail-channel (folder-name filter)
  `(make-mail-channel
    (mel:make-maildir-folder
     (merge-pathnames (concatenate 'string ,folder-name "/") *maildir-directory*))
    ,filter))

(defmacro rss-channel (name filter)
  `(make-rss-channel
    (merge-pathnames (make-pathname :name ,name :type "rss")
                     *persistent-state-directory*)
    ,filter))

;; The channels are listed in the order in which they are processed.
;; For each toot, the channel list is traversed until a channel's filter
;; responds T. The last channel is a catch-all, ensuring that each toot
;; ends up in exactly one channel.

(defvar *dispatcher*
  (make-toot-dispatcher 
   (list (;; Toots that mention me go to mail, so I don't miss them.
          ;; Toots that are private or direct also go to mail, to avoid
          ;; exposing them to a publicly accessible (though well hidden)
          ;; RSS feed.
          mail-channel "Mastodon"
                       (one-of #'mentions-me?
                               #'reply-to-me?
                               #'private?
                               #'direct?))

         ;; Toots that I have already seen in the last 7 days go to a
         ;; special RSS channel that I do not subscribe to. It also
         ;; receives toots that are Greek to me.
         (rss-channel "discard"
                      (one-of (in-registry? *registry*)
                              (language-in? '(:el))))

         ;; Toots tagged #emacs, and toots from Sacha Chua's
         ;; account for announcing Emacs-News posts, go to
         ;; a special Emacs feed.
         (rss-channel "emacs"
                      (one-of (account-in?
                               '("sachac@emacs.ch"))
                              (tag-in?
                               '("emacs"))))

         ;; Toots containing the substrings "fediverse" or
         ;; "activitpub" (case insensitive) go to a special
         ;; Fediverse feed.
         (rss-channel "fediverse"
                      (contains-substrings?
                       '("fediverse"
                         "activitypub")))

         ;; Everything else goes to "other", a feed I read occasionally
         ;; when I am bored.
         (rss-channel "other"
                      #'(lambda (status)
                          (declare (ignore status))
                          t)))))

;; Get the toots from the home timeline, starting after the last toot
;; that was processed during the last invocation.

(defvar *timeline*
  (timeline-from-id *mastodon-client* (max-id *registry*)))

(vom:info "Processing ~d toots" (length *timeline*))

;; Dispatch each toot into the channels, and add the toot to the registry.

(dolist (toot *timeline*)
  (register-event (tooter:account-name (tooter:account toot)) *counter*)
  (dispatch *dispatcher* toot)
  (register toot *registry*))

;; Flush the RSS channels, i.e. write them out to their RSS feed files.

(flush *dispatcher*)

;; Prune the registry to the last seven days, then save it.

(remove-before (local-time:timestamp- (local-time:now) 7 :day)
               *registry*)

(cl-store:store *registry* *registry-file*)

;; Save the event counter.

(cl-store:store *counter* *counter-file*)
