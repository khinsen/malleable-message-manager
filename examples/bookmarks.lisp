(ql:quickload :malleable-message-manager)
(ql:quickload :trivial-open-browser)

;; A simple in-browser view for bookmarks stored in a mail folder, one
;; message per bookmark.

(defpackage :bookmark-viewer
  (:use :cl :malleable-message-manager))

(in-package :bookmark-viewer)

;; Set *maildir-directory*. See maildir.lisp for details

(load
 (merge-pathnames #P".config/malleable-message-manager/maildir.lisp"
                  (user-homedir-pathname)))

;; The Bookmarks folder and its messages (sorted by date)

(defvar *bookmark-folder*
  (mel:make-maildir-folder
   (merge-pathnames "Bookmarks/" *maildir-directory*)))

(defvar *bookmarks*
  (sort (mel:messages *bookmark-folder*)
        #'< :key #'mel:date))

;; Create an HTML file listing the bookmarks from the Bookmarks folder

(defvar *html-filename*
  (merge-pathnames #P"Temp/bookmarks.html"
                   (user-homedir-pathname)))

(defvar *html-prefix*
  "<!doctype html>
<html lang=\"en-US\">
  <head>
    <meta charset=\"UTF-8\" />
    <title>Bookmarks</title>
  </head>
  <body><ul>
")

(defvar *html-entry-template*
  "<li><a href=\"~a\" target=\"_blank\">~a</a></li>
")

(defvar *html-suffix*
  "</ul></body>
</html>
")

;; Write the HTML file and open it in the default browser.

(with-open-file (output *html-filename*
                        :direction :output
                        :if-exists :supersede)
  (write-string *html-prefix* output)
  (dolist (bookmark *bookmarks*)
    (multiple-value-bind (title url)
        (title-and-url bookmark)
      (format output *html-entry-template* url title)))
  (write-string *html-suffix* output))

(trivial-open-browser:open-browser
 (format nil "file://~a" *html-filename*) )
