(ql:quickload :malleable-message-manager)

;; Follow subscribed conversations via mail messages
;;
;; "Subscription" doesn't exist in Mastodon. I use bookmarks
;; for this purpose. A subscription is defined by one of its
;; toots being bookmarked.
;;
;; Mail folders never store the same toot twice, so it is safe to run
;; this several times for the same bookmarked toot, in order to fetch
;; new replies.

(defpackage :mastodon-conversations
  (:use :cl :malleable-message-manager))

(in-package :mastodon-conversations)

;; Configuration for your Mastodon instance
;;
;; Sets *mastodon-instance-url*, *mastodon-account-name*, and
;; *mastodon-client*. See file mastodon-instance.lisp for details.

(load
 (merge-pathnames #P".config/malleable-message-manager/mastodon-instance.lisp"
                  (user-homedir-pathname)))

;; Set *maildir-directory*. See maildir.lisp for details

(load
 (merge-pathnames #P".config/malleable-message-manager/maildir.lisp"
                  (user-homedir-pathname)))

;; The mail folder in which conversations will be stored

(defvar *mail-folder*
  (-> (merge-pathnames #P"Mastodon/" *maildir-directory*)
      (mel:make-maildir-folder)))

;; Store a toot, with the complete reply tree containing it, in *mail-folder*.

;; The first version retrieves the reply tree from the user's instance. This
;; yields the same reply tree as the Web client.
(defun store-reply-tree (toot)
  (dolist (item (reply-tree toot))
    (-<> item
         (toot-as-message)
         (store-message-in-folder <> *mail-folder*))))

;; The user's instance often does not have all toots in the reply
;; tree. The second version grabs each tweet from the instance it was
;; posted on. This works only for public toots, and is generally more
;; fragile, which is why it is abandoned as soon as any error occurs.

(defun store-foreign-reply-tree (toot)
  (dolist (item (handler-case (reply-tree-from-origin-instances toot)
                  (error (c) (declare (ignore c))
                    nil)))
    (-<> item
         (toot-as-message)
         (store-message-in-folder <> *mail-folder*))))

;; Get the bookmarks.

(defvar *bookmarks* (tooter:collect-all-pages *mastodon-client*
                          (tooter:bookmarks *mastodon-client*)))

;; Store the reply tree for each bookmark. Both the local and the
;; foreign trees are retrieved and stored. This does not result in
;; extraneous messages because the message id of the stored toot
;; is independent of the instance it was obtained from.

(dolist (toot *bookmarks*)
  (format t "Processing ~a~%" toot)
  (store-reply-tree toot)
  (store-foreign-reply-tree toot))
