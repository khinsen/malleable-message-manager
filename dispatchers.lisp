;;;; Dispatch a toot into a chain of channels

(in-package #:malleable-message-manager)

(defclass toot-dispatcher ()
  ((channels :reader channels :initarg :channels)))

(defun make-toot-dispatcher (channels)
  (make-instance 'toot-dispatcher :channels channels))

(defgeneric dispatch (dispatcher toot))

(defmethod dispatch ((dispatcher toot-dispatcher) toot)
  (vom:debug "Dispatching toot ~a" (tooter:id toot))
  (vom:debug1 (toot-summary toot))
  (some #'(lambda (channel)
            (handler-case (maybe-put toot channel)
              (error (c)
                (vom:error "Error processing toot ~a~%~a~%~a~%"
                           (tooter:id toot)
                           c
                           (toot-summary toot)))))
        (channels dispatcher)))

(defgeneric flush (dispatcher))

(defmethod flush ((dispatcher toot-dispatcher))
  (dolist (channel (channels dispatcher))
    (handler-case (flush channel)
      (error (c)
        (vom:error "Error flushing channel ~a~%~a~%"
                   (filename channel)
                   c)))))

;;
;; Debug trace
;;

(defclass status-dispatch-trace ()
  ((dispatcher :reader trace-dispatcher :initarg :dispatcher)
   (status :reader trace-status :initarg :status)))

;;
;; View for toot submission, leading to a trace for debugging
;;

(hiv:defview 👀trace (dispatcher toot-dispatcher)
  (hiv:html-view :title "Trace" :priority 1
    (let ((toot-url-id (hiv:input-id)))
      (hiv:html
        (:div
         (:label :for toot-url-id (hiv:esc "Toot URL: "))
         (:input :type "text" :id toot-url-id)
         (hiv:str "&nbsp;")
         (hiv:eval-button "Trace"
                         (hiv:thunk
                          (let* ((url (hiv:get-input toot-url-id))
                                 (status (find-status-from-url url)))
                            (make-instance 'status-dispatch-trace
                                           :dispatcher dispatcher
                                           :status status)))))))))

;;
;; Views on the dispatch trace
;;

(hiv:defview 👀status (trace status-dispatch-trace)
  (-> trace
      trace-status
      👀content
      (hiv:rename :title "Status" :priority 1)) )

(hiv:defview 👀channels (trace status-dispatch-trace)
  (-> trace
      trace-dispatcher
      channels
      (hiv:multi-column-list-view
       :title "Channels"
       :priority 2
       :columns '("" "Channel")
       :display (list #'(lambda (c)
                          (if (funcall (filter c) (trace-status trace)) "✓" "❌"))
                      #'identity))) )
